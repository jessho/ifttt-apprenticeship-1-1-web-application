require "spec_helper"

describe PairingsController do
  describe "routing" do

    it "routes to #index" do
      get("/pairings").should route_to("pairings#index")
    end

    it "routes to #new" do
      get("/pairings/new").should route_to("pairings#new")
    end

    it "routes to #show" do
      get("/pairings/1").should route_to("pairings#show", :id => "1")
    end

    it "routes to #edit" do
      get("/pairings/1/edit").should route_to("pairings#edit", :id => "1")
    end

    it "routes to #create" do
      post("/pairings").should route_to("pairings#create")
    end

    it "routes to #update" do
      put("/pairings/1").should route_to("pairings#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/pairings/1").should route_to("pairings#destroy", :id => "1")
    end

  end
end
