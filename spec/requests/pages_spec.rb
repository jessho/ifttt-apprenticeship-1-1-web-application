require 'spec_helper'

describe "Pages" do

  describe "Home page" do
	  it "should have the content 'IFTTT'" do
	    visit '/pages/home'
	    page.should have_content('IFTTT')
	  end
  end
end
