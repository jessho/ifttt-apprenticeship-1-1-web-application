# == Schema Information
#
# Table name: pairings
#
#  id           :integer          not null, primary key
#  person_id    :integer
#  pair_id      :integer
#  pairing_date :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'spec_helper'

describe Pairing do
	
	before { @pairing = Pairing.new(person_id: 1, pair_id: 2, pairing_date: DateTime.now) }
	subject { @pairing }

	it { should respond_to(:person) }
	it { should respond_to(:pair) }
	it { should respond_to(:pairing_date)}

	it { should be_valid }

	describe "must have a person attribute" do
		before { @pairing.person_id = nil }
		it { should_not be_valid }
	end

	describe "must have a pair attribute" do
		before { @pairing.pair_id = nil }
		it { should_not be_valid }
	end

	describe "must have a pairing_date" do
		before { @pairing.pairing_date = nil }
		it { should_not be_valid }
	end

	describe "can't go on pairing with yourself" do
		before { @pairing.pair_id = @pairing.person_id }
		it { should_not be_valid }
	end

  # pending "add some examples to (or delete) #{__FILE__}"
end
