# == Schema Information
#
# Table name: memberships
#
#  id         :integer          not null, primary key
#  person_id  :integer
#  team_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Membership do
	
	before { @membership = Membership.new(person_id: 1, team_id: 1) }
	subject { @membership }

	it { should respond_to(:person)}
	it { should respond_to(:team)}

	it { should be_valid }

  # pending "add some examples to (or delete) #{__FILE__}"
end
