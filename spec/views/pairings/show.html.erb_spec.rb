require 'spec_helper'

describe "pairings/show" do
  before(:each) do
    @pairing = assign(:pairing, stub_model(Pairing,
      :person_id => 1,
      :pair_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
