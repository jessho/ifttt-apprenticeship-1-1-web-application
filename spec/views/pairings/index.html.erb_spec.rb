require 'spec_helper'

describe "pairings/index" do
  before(:each) do
    Person.create(name: "test1", email: "test1@email.com")
    Person.create(name: "test2", email: "test2@email.com")
    assign(:pairings, [
      stub_model(Pairing, {
        person_id: 1,
        pair_id: 2,
        pairing_date: DateTime.now }
      ),
      stub_model(Pairing, {
        person_id: 2,
        pair_id: 1,
        pairing_date: DateTime.now }
      ) 
    ])

  end

  it "renders a list of pairings" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "test1", :count => 2
    assert_select "tr>td", :text => "test2", :count => 2
  end
end
