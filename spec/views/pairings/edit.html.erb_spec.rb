require 'spec_helper'

describe "pairings/edit" do
  before(:each) do
    @pairing = assign(:pairing, stub_model(Pairing,
      :person_id => 1,
      :pair_id => 1
    ))
  end

  it "renders the edit pairing form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", pairing_path(@pairing), "post" do
      assert_select "input#pairing_person_id[name=?]", "pairing[person_id]"
      assert_select "input#pairing_pair_id[name=?]", "pairing[pair_id]"
    end
  end
end
