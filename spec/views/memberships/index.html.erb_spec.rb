require 'spec_helper'

describe "memberships/index" do
  before(:each) do
    assign(:teams, [
      stub_model(Team,
        :name => "test team",
        :id => 1
      ),
      stub_model(Team,
        :name => "test team 2",
        :id => 2
      )
    ])
    assign(:team_rosters, {
      1 => [],
      2 => []
    })
    assign(:memberships, [
      stub_model(Membership, {
        :person_id => 1,
        :team_id => 1 }
      ),
      stub_model(Membership, {
        :person_id => 1,
        :team_id => 2 }
      ) 
    ])
  end

  it "renders a list of memberships" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "h2", :text => 'test team', :count => 1
    assert_select "h2", :text => 'test team 2', :count => 1
  end
end
