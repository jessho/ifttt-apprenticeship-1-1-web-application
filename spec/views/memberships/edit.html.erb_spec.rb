require 'spec_helper'

describe "memberships/edit" do
  before(:each) do
    @membership = assign(:membership, stub_model(Membership,
      :person_id => 1,
      :team_id => 1
    ))
  end

  it "renders the edit membership form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", membership_path(@membership), "post" do
      assert_select "select#membership_person_id[name=?]", "membership[person_id]"
      assert_select "select#membership_team_id[name=?]", "membership[team_id]"
    end
  end
end
