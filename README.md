# Matchmaker: an app for generating IFTTT's 1+1 pairings

This application takes people in an organization, and automatically
generates the upcoming week's 1+1 pairings for teammates.

Every Friday evening, it will email participants with their upcoming 1+1.

Users of this application can add and remove people to the organization,
add and remove teams, and add and remove people to teams.

Additional features include: viewing 1+1 pairing histories on an
individual and team level.

# How to run this app

After cloning this repository:
* navigate to the corresponding directory
* bundle update
* bundle install
* start the rails server, and navigate to localhost:3000 in your favorite web browser.

On the homepage, you will see pre-seeded data for pairings along with links to people and teams (where you can add both).

# Application design

## Relational databases. 

People and teams have a many-to-many relationship, team membership is captured in a membership table.

People have many pairings. A pairing has one "owner" and one "pair," both of which are distinct people.

For more specifics on models, please refer to the [associated wiki](https://bitbucket.org/jessho/ifttt-apprenticeship-1-1-web-application/wiki).

## Site design. 

The front page features high-level information, specifically:
generating/viewing this week's pairings, and links to organization and team rorsters.

## Pairing algorithm.

My greedy algorithm is as follows:

* Generate all list of valid pairings
* Sort pairings by stalest first (time elapsed since the this 1+1 was assigned)
    * When people are new to a team, technically no time has elapsed since the
    previous 1+1. In this case, they're considered infinitely stale
* Traverse the list in order, and for each:
    * If both people in the pairing are available, assign the 1+1, and place both
    people in the unavailable list
    * Else, move to the next pairing

Note that I decided it was more important to ensure a new hire is placed
in a 1+1 than anything else. 1+1's are a mechanism for maintaining 
good relationships, teamwork, and cultural unity, thus it is critical
to integrate new people as quickly as possible.

Otherwise, this algorithm prioritizes "refreshing" relationships between
people who have not seen each other in a while (so people don't "lose track
of each other").

Brief runtime analysis:
* Generating all valid pairings is, at worst, O(n^2) when everyone's on the same team
* Checking pairing staleness is constant per query, so it is also O(n^2)
* Sorting is O(mlogm) where m is size of the list being sorted. So O(n^2 * logn)
* Finally, traversing a list is linear in list size
* Thus, overall the algorithm runs in O(n^2 * logn)

## Testing.

I performed basic testing, dividing my efforts into a few major components.

### People

I tested: 

* Email uniqueness for each person
* Names can be repeated
* Adding and deleting people

### Teams

I tested:

* People can be added to as many teams as they want
* People can be added only once to a team
* People can be removed from teams
* Deleting a team removes all people from that team
* Deleting a person removes that person from teams

### Pairings

I tested:

* 1+1 results for one team, even number of people
* 1+1 results for one team, odd number of people
* Same as above, checking frequency of pairings between teammates
* Same as above, checking that newly-added teammates get paired
* Same as above, but for two teams

### Email notification

I tested that emails assigned to be sent at a specific time work for my personal address (not commited in git for security reasons)