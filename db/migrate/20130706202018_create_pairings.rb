class CreatePairings < ActiveRecord::Migration
  def change
    create_table :pairings do |t|
      t.integer :person_id
      t.integer :pair_id
      t.datetime :pairing_date

      t.timestamps
    end
  end
end
