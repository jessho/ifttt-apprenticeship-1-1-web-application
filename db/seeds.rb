# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Dummy entries for 26 people
Person.create(name: 'Alice Alfa', email:'alice@email.com')
Person.create(name: 'Bob Bravo', email:'bob@email.com')
Person.create(name: 'Chris Charlie', email:'chris@email.com')
Person.create(name: 'Dan Delta', email:'dan@email.com')
Person.create(name: 'Emma Echo', email:'emma@email.com')
Person.create(name: 'Fred Foxtrot', email:'fred@email.com')
Person.create(name: 'Greg Golf', email:'greg@email.com')
Person.create(name: 'Hank Hotel', email:'hank@email.com')
Person.create(name: 'Ian India', email:'ian@email.com')
Person.create(name: 'Jane Juliet', email:'julia@email.com')
Person.create(name: 'Kate Kilo', email:'kate@email.com')
Person.create(name: 'Larry Lima', email:'larry@email.com')
Person.create(name: 'Mark Mike', email:'mark@email.com')
Person.create(name: 'Nate November', email:'nate@email.com')
Person.create(name: 'Owen Oscar', email:'owen@email.com')
Person.create(name: 'Pat Papa', email:'pat@email.com')
Person.create(name: 'Quinn Quebec', email:'quinn@email.com')
Person.create(name: 'Rick Romeo', email:'rick@email.com')
Person.create(name: 'Sam Sierra', email:'sam@email.com')
Person.create(name: 'Tim Tango', email:'tim@email.com')
Person.create(name: 'Una Uniform', email:'una@email.com')
Person.create(name: 'Vicky Victor', email:'victor@email.com')
Person.create(name: 'Wanda Whiskey', email:'wanda@email.com')
Person.create(name: 'Xena X-ray', email:'xena@email.com')
Person.create(name: 'Zack Zulu', email:'zack@email.com')

# Dummy entries for 6 teams
Team.create(name: 'Autobots')
Team.create(name: 'Avengers')
Team.create(name: 'Fantastic Four')
Team.create(name: 'Justice League')
Team.create(name: 'Watchmen')
Team.create(name: 'X-men')

# Dummy entries to fill 2 teams with members
Membership.create(person_id: Person.find(1).id, team_id: Team.find(1).id)
Membership.create(person_id: Person.find(2).id, team_id: Team.find(1).id)
Membership.create(person_id: Person.find(3).id, team_id: Team.find(1).id)
Membership.create(person_id: Person.find(4).id, team_id: Team.find(1).id)
Membership.create(person_id: Person.find(5).id, team_id: Team.find(2).id)
Membership.create(person_id: Person.find(6).id, team_id: Team.find(2).id)
Membership.create(person_id: Person.find(7).id, team_id: Team.find(2).id)