class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :generate_valid_pairings
  helper_method :sort_valid_pairings_by_staleness
  helper_method :peope_are_unavailable?
  helper_method :assign_pairings
  # generate list of all valid pairings
	# sort list by time of last pairing (oldest first)
	# make sure new teammates (no pairings ever before) shift to top
	# pick off elements from the front, if participants are available.
	# make new pairing
	# add participants to unavailable list
	# repeat

	def generate_valid_pairings(valid_pairings)
		@teams = Team.all
		@teams.each do |team|
			members = team.people
			combos = members.combination(2)

			combos.each do |pairing|
				person_id = pairing[0].id
				pair_id = pairing[1].id
				valid_pairings << Pairing.new(person_id: person_id, pair_id: pair_id)
			end
		end
		valid_pairings
	end

	# Takes the array of valid_pairings and sorts it by stalest first.
	# A given pairing is staler if their most recent 1+1 happened earlier 
	# than another pairing 
	def sort_valid_pairings_by_staleness(valid_pairings)
		valid_pairings.sort! { |a,b| most_recent_pairing(a) <=> most_recent_pairing(b) }
	end

	# Takes a pairing of two people as a parameter. Returns ActiveSupport::TimeWithZone 
	# object representing their most recent 1+1
	def most_recent_pairing(pairing) 
		
		first_id = pairing.person_id
		second_id = pairing.pair_id
		most_recent = Pairing.last(
			conditions: {person_id: [first_id, second_id], pair_id: [first_id, second_id]})
		
		if most_recent.nil?	# if they've never been paired, default is 5 years ago
			5.years.ago
		else
			most_recent.pairing_date
		end
	end

	# Goes through array of valid_pairings, assigns them if participants are available.
	def assign_pairings(valid_pairings, unavailable_people, timestamp)
		valid_pairings.each do |pairing|
			if people_are_available?(pairing, unavailable_people)
				Pairing.create(
					person_id: pairing.person_id, 
					pair_id: pairing.pair_id, 
					pairing_date: timestamp)
				unavailable_people.add(pairing.person)
				unavailable_people.add(pairing.pair)
			end
		end
	end

	# Given a pairing, checks the set of unavailable_people, and returns true
	# if both are available for pairing
	def people_are_available?(pairing, unavailable_people)
		if unavailable_people.member?(pairing.person)
			false
		elsif unavailable_people.member?(pairing.pair)
			false
		else
			true
		end
	end


end
