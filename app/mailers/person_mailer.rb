class PersonMailer < ActionMailer::Base
  default from: "jessho.throwaway@gmail.com", subject: "Your weekly 1+1 pairing"

  def weekly_pairing(pairing)
  	@pairing = pairing
  	@person = pairing.person
  	@pair = pairing.pair
  	mail(to: "#{@person.name} <#{@person.email}>, #{@pair.name} <#{@pair.email}>")
  end

  def all_pairings
  	puts "Emailing out pairings"
  	@latest = Pairing.last
  	@all_pairings = Pairing.where(pairing_date: @latest.pairing_date)
  	@all_pairings.each do |pairing|
  		weekly_pairing(pairing).deliver
  	end
  end

  # def print_some_stuff
  # 	puts "hello world"
  # end

end
