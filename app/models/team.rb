# == Schema Information
#
# Table name: teams
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Team < ActiveRecord::Base
  attr_accessible :name

  has_many :memberships, dependent: :destroy
  has_many :people, through: :memberships

  validates :name, presence: true,
                   uniqueness: { case_sensitive: false }
end
