# == Schema Information
#
# Table name: people
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Person < ActiveRecord::Base
  attr_accessible :email, :name

  has_many :memberships, dependent: :destroy
  has_many :teams, through: :memberships

  has_many :pairings
  has_many :pairs, through: :pairings

  has_many :inverse_pairings, class_name: "Pairing", foreign_key: "pair_id"
  has_many :inverse_pairs, through: :inverse_pairings, source: :person
  
  validates :name, presence: true
  validates :email, presence: true,
                    uniqueness: { case_sensitive: false }

end
