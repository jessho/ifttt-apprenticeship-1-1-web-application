# == Schema Information
#
# Table name: pairings
#
#  id           :integer          not null, primary key
#  person_id    :integer
#  pair_id      :integer
#  pairing_date :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Pairing < ActiveRecord::Base
  attr_accessible :pair_id, :pairing_date, :person_id

  belongs_to :person
  belongs_to :pair, class_name: "Person"

  validates :person_id, presence: true
  validates :pair_id, presence: true
  validates :pairing_date, presence: true

  validate :cannot_pair_self

  def cannot_pair_self
  	if self.person_id == self.pair_id
  		errors[:base] << 'You cannot pair with yourself on a 1+1.'
  	end
  end
end
