# == Schema Information
#
# Table name: memberships
#
#  id         :integer          not null, primary key
#  person_id  :integer
#  team_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Membership < ActiveRecord::Base
  attr_accessible :person_id, :team_id

  belongs_to :person
  belongs_to :team

  validates :person_id, presence: true
  validates :team_id, presence: true
  validates :person_id, :uniqueness => {:scope => :team_id}

end
